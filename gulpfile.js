var gulp = require('gulp');
var browserSync = require('browser-sync').create();

// HTML
gulp.task('html', function(){
  gulp.src('./html/*.html')
  .pipe(gulp.dest('./public'))
  .pipe(browserSync.stream());
})

// CSS
gulp.task('css', function(){
  gulp.src('./themes/css/*.css')
  .pipe(gulp.dest('./public/themes/css'))
  .pipe(browserSync.stream());
})

// Img
gulp.task('img', function(){
  gulp.src('./themes/images/*.png')
  .pipe(gulp.dest('./public/themes/images'))
})

// Js
gulp.task('js', function(){
  gulp.src('./themes/js/*.js')
  .pipe(gulp.dest('./public/themes/js'))
  .pipe(browserSync.stream());
})

// Static server
gulp.task('sync', ['html', 'css', 'js'], function() {
  browserSync.init({
    server: {
      baseDir: "./public"
    }
  });

  gulp.watch('./html/*.html', ['html']);
  gulp.watch('./themes/css/*.css', ['css']);
  gulp.watch('./themes/js/*.js', ['js']);
});

gulp.task('default', ['sync', 'img'])